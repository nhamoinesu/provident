# Flask Packages
from flask import Flask,render_template,request,url_for
from flask_bootstrap import Bootstrap 
from flask_uploads import UploadSet,configure_uploads,IMAGES,DATA,ALL
from flask_sqlalchemy import SQLAlchemy 

from werkzeug import secure_filename
import os
import datetime
import time
#
#import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
#from sklearn.metrics import mean_squared_error
#from sklearn.preprocessing import MinMaxScaler
#from keras.models import Sequential
#from keras.layers import Dense
#from keras.layers import LSTM
from math import sqrt
from matplotlib import pyplot
# load and plot dataset
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
import time
import datetime
import numpy as np
#from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics
import matplotlib.pyplot as plt

# EDA Packages
import pandas as pd 
import numpy as np 
import pickle
import io
import random
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

# ML Packages
#from sklearn import model_selection
#from sklearn.linear_model import LogisticRegression
#from sklearn.tree import DecisionTreeClassifier
#from sklearn.neighbors import KNeighborsClassifier
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
#from sklearn.naive_bayes import GaussianNB
#from sklearn.svm import SVC


# ML Packages For Vectorization of Text For Feature Extraction
#from sklearn.feature_extraction.text import CountVectorizer
#from sklearn.feature_extraction.text import TfidfVectorizer

from AlertsGenerator import AlertsGenerator
from ExcelFilesReader import ExcelFilesReader

alerts = AlertsGenerator('out_all_files_except_2016b_and_2017.csv','out_2017.csv')
f_reader = ExcelFilesReader()

#pickle_in = open("yellow_model.pickle","rb")
#yellow_dict = pickle.load(pickle_in)


#pickle_in = open("red_model.pickle","rb")
#red_dict = pickle.load(pickle_in)
cols = ['LGA', 'State', 'Month', 'Year', 'Disease Name',
       'CO_0to28D', 'CO_1to11M', 'CO_12to59M', 'CO_5to9Y', 'CO_10to19Y',
       'CO_20to40Y', 'CO_Ovr40Y', 'CO_Total', 'CI_0to28D', 'CI_1to11M',
       'CI_12to59M', 'CI_5to9Y', 'CI_10to19Y', 'CI_20to40Y', 'CI_Ovr40Y',
       'CI_Total', 'CIO_Total', 'DIO_0to28D', 'DIO_1to11M', 'DIO_12to59M',
       'DIO_5to9Y', 'DIO_10to19Y', 'DIO_20to40Y', 'DIO_Ovr40Y', 'DIO_Total']


app = Flask(__name__)
Bootstrap(app)
db = SQLAlchemy(app)

# Configuration for File Uploads
files = UploadSet('files',ALL)
app.config['UPLOADED_FILES_DEST'] = 'static/uploadsDB'
configure_uploads(app,files)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///static/uploadsDB/filestorage.db'
base_path = os.path.dirname(__file__)

# Saving Data To Database Storage
class FileContents(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(300))
    modeldata = db.Column(db.String(300))
    data = db.Column(db.LargeBinary)

@app.route('/drop_d', methods=['GET'])
def dropdown():
    diseases = alerts.disease_names_list_2018
    return diseases
    #colours = ['Red', 'Blue', 'Black', 'Orange']
    #return render_template('test.html', colours=colours)
    
@app.route('/plot.epe')
def plot_epe_png():
    fig = create_epe_figure()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def create_epe_figure():
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    epe_chart = [i for i in range(1,13)] #get_epe_test_charts_data('','Malaria','CI_Total')
    xs = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'] #range(100)
    ys = epe_chart #[random.randint(1, 50) for x in xs]
    axis.plot(xs, ys)
    return fig    

@app.route('/plot.bad')
def plot_bad_png():
    fig = create_badagry_figure()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')

def create_badagry_figure():
    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    bad_chart = [i for i in range(1,13)] #get_badagry_test_charts_data('','Malaria','CI_Total')
    xs = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'] #range(100)
    ys = bad_chart #[random.randint(1, 50) for x in xs]
    axis.plot(xs, ys)
    return fig 

@app.route('/')
def index():
    print(base_path)
    return render_template('index.html')

@app.route('/chart')
def chart():
    labels = ["January","February","March","April","May","June","July","August"]
    values = [10,9,8,7,6,4,7,8]
    return render_template('chart.html', values=values, labels=labels)

# Route for our Processing and Details Page
@app.route('/dataupload',methods=['GET','POST'])
def dataupload():
    if request.method == 'POST' and 'xls_data' in request.files:
        file = request.files['xls_data']
        filename = secure_filename(file.filename)
        uploads_folder = os.path.join(base_path, "static", "uploadsDB")
        # # os.path.join is used so that paths work in every operating system
        # # file.save(os.path.join("wherever","you","want",filename))
        file.save(os.path.join(uploads_folder,filename))
        fullfile = os.path.join(uploads_folder,filename)
        #dataset_check = pd.read_excel(fullfile)
        dataset_check = parse_test_file(fullfile)
        fullfile1 = dataset_check.head()
        ### For Time
        date = str(datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S"))

        # # Exploratory Data Analysis function
        #df = pd.read_excel(fullfile)
        df = parse_test_file(fullfile)
        df_size = df.size
        df_shape = df.shape
        df_columns = list(df.columns)
        df_targetname = df[df.columns[-1]].name
        df_featurenames = df_columns[0:-1] # select all columns till last column
        df_Xfeatures = df.iloc[:,0:-1] 
        df_Ylabels = df[df.columns[-1]] # Select the last column as target
        #=======
    
    diseases = alerts.disease_names_list_2018
    #epe_chart = get_epe_test_charts_data('','Malaria','CIO_Total')
    #bad_chart = get_badagry_test_charts_data('','Malaria','CIO_Total')
    red_month = report_monthly_red(fullfile,alerts.red_dict)

    # In[7]:
    #yellow =[]
    yellow = report_monthly_yellow(fullfile,alerts.yellow_dict)
    #yellow = str(yellow).split(',')
    yellow_month = yellow
    #fig_epe = plot_epe_png()
    #fig_bad = plot_bad_png()
    
    return render_template('details.html',filename=fullfile,date=date,
        df_size=df_size,
        df_shape=df_shape,
        df_columns =df_columns,
        df_targetname =df_targetname,
        fullfile = fullfile,
        dfplot = df.head(),
        red_month = red_month,
        yellow_month = yellow_month,
        #epe_chart = epe_chart,
        #bad_chart = bad_chart,
        df_featurenames = df_featurenames,
        diseases = diseases
        #fig_bad = fig_bad
        )


def create_badagry_df():

    month_list = ['LGA_Badagry_IDSR_003_January2018.xlsx',
            'LGA_Badagry_IDSR_003_February2018.xlsx',    
    'LGA_Badagry_IDSR_003_March2018.xlsx',
    'LGA_Badagry_IDSR_003_April2018.xlsx',
    'LGA_Badagry_IDSR_003_May2018.xlsx',
    'LGA_Badagry_IDSR_003_June2018.xlsx',
    'LGA_Badagry_IDSR_003_July2018.xlsx',              
    'LGA_Badagry_IDSR_003_August2018.xlsx',
    'LGA_Badagry_IDSR_003_September2018.xlsx',              
    'LGA_Badagry_IDSR_003_October2018.xlsx',
    'LGA_Badagry_IDSR_003_November2018.xlsx',
    'LGA_Badagry_IDSR_003_December2018.xlsx']


    df_list_18 = []
    for i in month_list:
    
        xls_18 = f_reader.read_excel_file(i)
        sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
        df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
        df_list_18.append(df_apr_18_temp)
        
    epe_all_2018_df = f_reader.combine_df(df_list_18)
    epe_all_2018_df.to_csv('out_bad_all_2018.csv', sep=',') 
    
    return alerts.read_test_data('out_bad_all_2018.csv')

def create_epe_df():
    month_list =['LGA_Epe_IDSR_003_January2018.xlsx',
        'LGA_Epe_IDSR_003_February2018.xlsx',
        'LGA_Epe_IDSR_003_March2018.xlsx',
        'LGA_Epe_IDSR_003_April2018.xlsx',
        'LGA_Epe_IDSR_003_May2018.xlsx',
        'LGA_Epe_IDSR_003_June2018.xlsx',
        'LGA_Epe_IDSR_003_July2018.xlsx',
        'LGA_Epe_IDSR_003_August2018.xlsx',
        'LGA_Epe_IDSR_003_September2018.xlsx',
        'LGA_Epe_IDSR_003_October2018.xlsx',
        'LGA_Epe_IDSR_003_November2018.xlsx',
        'LGA_Epe_IDSR_003_December2018.xlsx']

    df_list_18 = []
    for i in month_list:
    
        xls_18 = f_reader.read_excel_file(i)
        sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
        df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
        df_list_18.append(df_apr_18_temp)
        
    epe_all_2018_df = f_reader.combine_df(df_list_18)
    epe_all_2018_df.to_csv('out_epe_all_2018.csv', sep=',') 
    
    return alerts.read_test_data('out_epe_all_2018.csv')

def get_epe_test_charts_data(filename,disease_name,column_name):
    df = create_epe_df()
    df = df.loc[df['Disease Name'] == disease_name]
    return df['Month'].values,df[column_name].values
    
    
def get_badagry_test_charts_data(filename,disease_name,column_name):
    df = create_badagry_df()
    df = df.loc[df['Disease Name'] == disease_name]
    return df['Month'].values,df[column_name].values


def parse_test_file(file):
    #xls_18 = f_reader.read_excel_file(file)
    #sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
    #df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
    #df_apr_18_temp.to_csv('test_file_2018.csv', sep=',')
    df1 = pd.read_csv(file)
    standard_df_test_2018 = df1[cols]
    return standard_df_test_2018

def report_monthly_yellow(file,y_dict):
    standard_df_test_2018 = parse_test_file(file)
    lga = list(set(standard_df_test_2018.LGA))[0]
    month = list(set(standard_df_test_2018.Month))[0]
    year = list(set(standard_df_test_2018.Year))[0]
    std_factor = 1.5
    #df_tuple = extract_disease_syndrome_dfs(alerts.standard_df_2012_16, standard_df_test_2018)
    #return alerts.get_monthly_yellow_alert(df_tuple[0], df_tuple[1],lga,year,std_factor)
    return alerts.get_monthly_yellow_alert(y_dict,standard_df_test_2018,lga,year,std_factor)
   
def report_monthly_red(file,r_dict,cx_total='CIO_Total'):
    standard_df_test_2018 = parse_test_file(file)
    lga = list(set(standard_df_test_2018.LGA))[0]
    month = list(set(standard_df_test_2018.Month))[0]
    year = list(set(standard_df_test_2018.Year))[0]
    std_factor = 1.5
    #print(lga)
    #df_tuple = extract_disease_syndrome_dfs(alerts.standard_df_2012_16, standard_df_test_2018)
    return alerts.get_monthly_red_alerts(r_dict, standard_df_test_2018,lga,cx_total,alerts.disease_names_list,alerts.disease_names_list_2018,year,std_factor)
    #return alerts.get_monthly_red_alerts(red_dict, standard_df_test_2018,lga,alerts.disease_names_list,alerts.disease_names_list_2018,2018,1.5)
    #return alerts.get_monthly_red_alerts(df_tuple[0], df_tuple[1],lga,'CIO_Total',alerts.disease_names_list,alerts.disease_names_list_2018,year,std_factor)


if __name__ == '__main__':
    app.run(debug=True)
