import pandas as pd
import statistics
import pandas as pd
import matplotlib.pyplot as plt
from pandas import datetime
from math import sqrt
from matplotlib import pyplot
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
import time
import datetime
import numpy as np
from math import sqrt
import statistics
class Model:

    def __init__(self):

    
        self.disease_tuple_list = {}
        self.disease_tuple_list['CSM'] = '2.   CSM' 
        self.disease_tuple_list['Cholera'] = '3. Cholera' 
        self.disease_tuple_list['Diarrhoea (with blood)'] = '6.   Diarrhoea (with blood)'
        self.disease_tuple_list['Diarrhoea (with blood)'] = '6.   Diarrhoea (with blood)'
        self.disease_tuple_list['Diarrhoea with dehydration(5yrs)'] = '5.   Diarrhoea (Watery without blood)'
        self.disease_tuple_list['Lassa fever (Viral hemorrhagic fever)'] ='10.  Lassa fever (Viral hemorrhagic fever)'
        self.disease_tuple_list['Malaria'] = '13a) Malaria '
        self.disease_tuple_list['Malaria (Pregnant Women)'] = '13c) Malaria (Pregnant Women)'
        self.disease_tuple_list['Malaria (severe)'] = '13b) Malaria (severe)'
        self.disease_tuple_list['Measles'] = '14. Measles'
        self.disease_tuple_list['Pneumonia ? 5yrs'] = '20. Pneumonia'
        
        self.disease_names_list = ['10.  Lassa fever (Viral hemorrhagic fever)',
                '14. Measles','3. Cholera','2.   CSM','5.   Diarrhoea (Watery without blood)',
                '6.   Diarrhoea (with blood)','13a) Malaria ','13b) Malaria (severe)',
                '13c) Malaria (Pregnant Women)','20. Pneumonia']
        self.disease_names_list_2017 = ['CSM','Cholera','Diarrhoea (with blood)',
                'Diarrhoea with dehydration (< 5yrs)','Lassa fever (Viral hemorrhagic fever)',
             'Malaria ','Malaria (Pregnant Women)','Malaria (severe)','Measles','Pneumonia (< 5yrs)']

        self.disease_names_list_2018 = ['CSM', 'Cholera', 'Diarrhoea (with blood)', 'Diarrhoea with dehydration(5yrs)',
                     'Lassa fever (Viral hemorrhagic fever)', 'Malaria','Malaria (Pregnant Women)',
                     'Malaria (severe)', 'Measles', 'Pneumonia ? 5yrs']
        
        self.age_grp_tuple_list = [('CI_Total','CI_0to28D'),('CI_Total','CI_1to11M'), ('CI_Total','CI_12to59M'), ('CI_Total','CI_5to9Y'), ('CI_Total','CI_10to19Y'), ('CI_Total','CI_20to40Y'),('CI_Total','CI_Ovr40Y'),
                            ('CO_Total','CO_0to28D'),('CO_Total','CO_1to11M'), ('CO_Total','CO_12to59M'), ('CO_Total','CO_5to9Y'), ('CO_Total','CO_10to19Y'), ('CO_Total','CO_20to40Y'),('CO_Total','CO_Ovr40Y')]



    def read_test_data(self,test_file):
        
        data_2018_df = pd.read_csv(test_file)
        if 'Epe' in data_2018_df.LGA.values:
            standard_df_2018_values = data_2018_df[self.epe_columns].values
        else:
            standard_df_2018_values = data_2018_df[self.badagry_columns].values
        standard_df_2018_columns = self.new_standard_cols_2012_16
        standard_df_2018 = pd.DataFrame(data=standard_df_2018_values, columns = standard_df_2018_columns)
        return standard_df_2018
    
    def create_dates(self,df):
        numeric_month = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
        years = df['Year'].values 
        months = df['Month'].values
        dt = [(datetime.datetime(year= years[i], month = numeric_month[months[i]], day = 1)).date() for i in range(0,len(years))]
        #for i in range(0,len(years)):
            #dt.append((datetime.datetime(year= years[i], month = numeric_month[months[i]], day = 1)).date())
        return dt
    
    def create_ts(self,df):
        df['ds'] = pd.DatetimeIndex(df['ds'])
        return df
    
    def extract_year(self,df,year):
        return df.loc[df['Year'] == year]

    def extract_year_2017(self,df,year):
        return df.loc[df['Year'] == year]

    def extract_lga_and_disease_and_year(self,df,lga_name,disease_name,year):
        temp = self.extract_lga_and_disease(df,lga_name,disease_name)
        return self.extract_year(temp,year)
    #extract_lga_and_year_and_disease_name_2017

    def extract_lga_and_disease_and_year_2017(self,df,lga_name,disease_name,year):
        temp = self.extract_lga_and_disease_2017(df,lga_name,disease_name)
        return self.extract_year_2017(temp,year)
 
    #def extract_lga_and_disease_and_year_2017
    def extract_lga_and_disease_and_year_2017(self,df,lga_name,disease_name,year):
        temp = self.extract_lga_and_disease_2017(df,lga_name,disease_name)
        #print(lga_name)
        return self.extract_year_2017(temp,year)

    def extract_lga_2017(self,dframe,lga_name):
        return dframe.loc[dframe['LGA'] == lga_name]

    def extract_lga_and_disease_2017(self,dframe,lga_name,disease_name):
        temp_lga = self.extract_lga_2017(dframe,lga_name)
        return self.extract_disease_type_2017(temp_lga,disease_name)

    def extract_disease_type(self,dframe,disease_name):
        return dframe.loc[dframe['Disease Name'] == disease_name]
    
    #agege_malaria_df = extract_lga_and_disease(standard_df_2012_16,'Agege','13a) Malaria ')
    def extract_lga_and_disease(self,dframe,lga_name,disease_name):
        temp_lga = self.extract_lga(dframe,lga_name)
        return self.extract_disease_type(temp_lga,disease_name)

    def extract_lga_and_syndrome(self,dframe,lga_name,syndrome_list):
        temp_lga = self.extract_lga(dframe,lga_name)
        for synd in syndrome_list:
            temp = pd.concat([temp, self.extract_disease_type(temp_lga,synd),malaria_13c])
        return temp
    
    def extract_lga(self,dframe,lga_name):
        return dframe.loc[dframe['LGA'] == lga_name]
    
    def plot_disease_graph(self,data):
        pyplot.plot(data)
        
    def add_value(self,df,value, cols):
        x = df[cols].values 
        y=np.asfarray(x,int)
        df[cols] = y + value
        return df
   

    def extract_age_grp(self,dframe,grp):
        return dframe[grp] 
    
    def extract_lga_and_disease_and_age_grp_lga(self,df,lga,disease_name,grp):
        temp = self.extract_lga_and_disease(df,lga_name,disease_name)
        return self.extract_age_grp(temp,grp)
    
    def extract_data_for_a_given_year(self,df,yr):
        return df.loc[df['Year'] == yr]
    
    def add_one(self,df,age_grp,total):
        temp = df[['Month','Year',age_grp,total]].iloc[0:48] 
        #temp['ds'] = self.create_dates(temp)
        temp[age_grp] =  (np.asfarray(temp[age_grp],int)+1.0)/(temp[total]+7.0)
        return temp
    
    def add_one_with_smoothing(self,df,age_grp,total):
        temp = df[['Month','Year',age_grp,total]].iloc[0:48] 
        #temp['ds'] = self.create_dates(temp)
        temp[age_grp] =  (np.asfarray(temp[age_grp],int)+1.0)/(temp[total]+7.0)
        temp[age_grp] = temp[age_grp].rolling(window=3,min_periods=1).mean().values
        return temp
    
    def extract_year_and_normalised(self,df,year,age_grp):
        temp = df.loc[df['Year'] == year]
        return temp[age_grp].values
    
    def calculate_average_and_std(self,df12, df13, df14, df15):
        mean_list = [statistics.mean([df12[i],df13[i],df14[i],df15[i]]) for i in range(12)]
        std_list = [statistics.stdev([df12[i],df13[i],df14[i],df15[i]]) for i in range(12)]
        #for i in range(12):
            #mean_list[i] = statistics.mean([df12[i],df13[i],df14[i],df15[i]])
            #std_list[i] = statistics.stdev([df12[i],df13[i],df14[i],df15[i]])
        return np.array(mean_list), np.array(std_list) 
    
    #plot_with_alerts(data_df,data_2017_df,'Agege','13a) Malaria ','Malaria ','CO_12to59M','CO_Total','2017',1.5)
  

    def create_dates_2017(self,df):
        numeric_month = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
        years = df['Year'].values 
        months = df['Month'].values
        dt = [(datetime.datetime(year= years[i], month = numeric_month[months[i]], day = 1)).date() for i in range(0,len(years))]
        #for i in range(0,len(years)):
            #dt.append((datetime.datetime(year= years[i], month = numeric_month[months[i]], day = 1)).date())
        return dt
        
    def extract_disease_type_2017(self,dframe,disease_name):
        return dframe.loc[dframe['Disease Name'] == disease_name]

    def extract_lga_and_disease_2017(self,dframe,lga_name,disease_name):
        temp_lga = self.extract_lga_2017(dframe,lga_name)
        return self.extract_disease_type(temp_lga,disease_name)

    def extract_lga_and_syndrome_2017(self,dframe,lga_name,syndrome_list):
        temp_lga = self.extract_lga_2017(dframe,lga_name)
        for synd in syndrome_list:
            temp = pd.concat([temp, self.extract_disease_type(temp_lga,synd),malaria_13c])
        return temp
    
    def extract_lga_2017(self,dframe,lga_name):
        return dframe.loc[dframe['LGA'] == lga_name]
    
    def plot_disease_graph_2017(self,data):
        pyplot.plot(data)     
        
        
    def add_one_2017(self,df,age_grp,total):
        temp = df[['Month','Year',age_grp,total]].iloc[0:48] 
        #temp['ds'] = create_dates_2017(temp)
        temp[age_grp] =  (np.asfarray(temp[age_grp],int)+1.0)/(temp[total]+7.0)
        return temp
    
    def add_one_with_smoothing_2017(self,df,age_grp,total):
        temp = df[['Month','Year',age_grp,total]].iloc[0:48] 
        #temp['ds'] = create_dates_2017(temp)
        temp[age_grp] =  (np.asfarray(temp[age_grp],int)+1.0)/(temp[total]+7.0)
        temp[age_grp] = temp[age_grp].rolling(window=3,min_periods=1).mean().values
        return temp
    
    def extract_year_and_normalised_2017(self,df,year,age_grp):
        temp = df.loc[df['Year'] == year]
        return temp[age_grp].values
    
    def calculate_average_and_std_2017(self,df12, df13, df14, df15):
        #mean_list = [0 for i in range(12)]
        #std_list = [0 for i in range(12)]
        mean_list = [statistics.mean([df12[i],df13[i],df14[i],df15[i]]) for i in range(12)]
        std_list = [statistics.stdev([df12[i],df13[i],df14[i],df15[i]])  for i in range(12)]
        
        #for i in range(12):
            #mean_list[i] = statistics.mean([df12[i],df13[i],df14[i],df15[i]])
            #std_list[i] = statistics.stdev([df12[i],df13[i],df14[i],df15[i]])
        return np.array(mean_list), np.array(std_list)         
        
    def get_months_of_the_year(self,df,lga):
        temp= df.loc[df['LGA'] == lga]
        temp= temp.loc[temp['Disease Name'] == '13a) Malaria ']
        temp = temp.loc[temp['Year'] == 2012]
        return temp['Month'].values

    def calculate_bounds(self,df,age_grp,grp_total):
        yr_2012 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2012,age_grp)
        yr_2013 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2013,age_grp)
        yr_2014 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2014,age_grp) 
        yr_2015 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2015,age_grp)

        model_mean, model_std = self.calculate_average_and_std(yr_2012,yr_2013,yr_2014,yr_2015)

        return model_mean, model_std

   

    def calculate_yellow_model_bounds(self,df,lga):
        
        
        #lower_list,upper_list = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
        age_grp_tuple_list = [('CI_Total','CI_0to28D'),('CI_Total','CI_1to11M'), ('CI_Total','CI_12to59M'), ('CI_Total','CI_5to9Y'), ('CI_Total','CI_10to19Y'), ('CI_Total','CI_20to40Y'),('CI_Total','CI_Ovr40Y'),
                            ('CO_Total','CO_0to28D'),('CO_Total','CO_1to11M'), ('CO_Total','CO_12to59M'), ('CO_Total','CO_5to9Y'), ('CO_Total','CO_10to19Y'), ('CO_Total','CO_20to40Y'),('CO_Total','CO_Ovr40Y')]
                

        dict_of_models= {}
        for k,v in self.disease_tuple_list.items():
            agege_malaria_df = self.extract_lga_and_disease(df,lga,v)
            dict_of_models[k] = [(j[1],self.calculate_bounds(agege_malaria_df,j[1],j[0])) for j in age_grp_tuple_list]
            #bounds_tuple = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
            #dict_of_models[k] = (bounds_tuple[0], bounds_tuple[1])
            #print(bounds_tuple)

        return dict_of_models
    
##***********************************************************
#def get_months_of_the_year
    def get_months_of_the_year(self,df,lga):
        temp= df.loc[df['LGA'] == lga]
        temp= temp.loc[temp['Disease Name'] == '13a) Malaria ']
        temp = temp.loc[temp['Year'] == 2012]
        return temp['Month'].values

    def calculate_bounds(self,df,age_grp,grp_total,std_factor=1.0):
        malaria_agege_2012_CI_12to59M = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2012,age_grp)
        malaria_agege_2013_CI_12to59M = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2013,age_grp)
        malaria_agege_2014_CI_12to59M = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2014,age_grp) 
        malaria_agege_2015_CI_12to59M = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2015,age_grp)

        co_12to59m_mean, co_12to59m_std = self.calculate_average_and_std(malaria_agege_2012_CI_12to59M,malaria_agege_2013_CI_12to59M,malaria_agege_2014_CI_12to59M, malaria_agege_2015_CI_12to59M)

        lower_bound = np.array(co_12to59m_mean) - np.array(co_12to59m_std)*std_factor
        upper_bound = np.array(co_12to59m_mean) + np.array(co_12to59m_std)*std_factor

        return lower_bound, upper_bound

    def check_bounds(self,value,lower,upper):
        #return (float(value) < float(lower)) or (float(value) > float(upper))
        return (float(value) > float(upper))
    
    def get_breach_list(self,values,lower_list,upper_list):
        #breach_list = []
        temp = [i for i in range(len(values)) if self.check_bounds(values[i],lower_list[i],upper_list[i])]
        return temp
        #for i in range(len(values)):
            #if self.check_bounds(values[i],lower_list[i],upper_list[i]):
                #breach_list.append(i)
        #return breach_list 
 

    def get_breach_list_one_month(self,values,lower_list,upper_list,pos):
        breach_list = []
        #for i in range(len(values)):
        if self.check_bounds(values[0],lower_list[pos],upper_list[pos]):
                breach_list.append(0)
        return breach_list 

    
    def extract_lga_and_year_and_disease_name(self,df,lga,year,disease_name):
        temp = self.extract_lga(df,lga)
        temp = self.extract_year(temp,year)
        temp = self.extract_disease_type(temp,disease_name)
        return temp  

   #plot_with_alerts(data_df,data_2017_df,'Agege','13a) Malaria ','Malaria ','CO_12to59M','CO_Total','2017',1.5)

 
    def count_one_month_with_alerts(self,df,df_2017,lga,disease_name,disease_name_2017,age_grp,grp_total,target_year,std_factor=1.0):
        agege_malaria_df_2017 = self.extract_lga_and_disease_2017(df_2017,lga,disease_name_2017)
        agege_malaria_df = self.extract_lga_and_disease(df,lga,disease_name)
        malaria_agege_2012 = self.extract_lga_and_year_and_disease_name(df,lga,2012,disease_name)
     
        #plot_std_graph_smoothed_2017(agege_malaria_df,agege_malaria_df_2017.iloc[0:12],'CO_12to59M','CO_Total',1.0)
        lower_list,upper_list = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
        #extract_year_and_normalised_2017(add_one_2017(df_2017,age_grp,grp_total),year,age_grp)
        malaria_agege_2012_CI_12to59M_2017 = self.extract_year_and_normalised_2017(self.add_one_2017(agege_malaria_df_2017.iloc[0:12],age_grp,grp_total),target_year,age_grp)
        values = malaria_agege_2012_CI_12to59M_2017[0:12]
        #print(values)
        x = self.get_months_of_the_year(df,lga)
        #print(x)
        xpos = self.make_month_dict()[list(set(df_2017.Month))[0]]
        b_list = self.get_breach_list(values,lower_list,upper_list)
        return lga, age_grp, disease_name, target_year, len(b_list)

    
    #def plot_one_month_with_alerts_2018
    def count_one_month_with_alerts_2018(self,df,df_2017,lga,disease_name,disease_name_2017,age_grp,grp_total,target_year,std_factor=1.0):
        agege_malaria_df_2017 = self.extract_lga_and_disease_2017(df_2017,lga,disease_name_2017)
        agege_malaria_df = self.extract_lga_and_disease(df,lga,disease_name)
        malaria_agege_2012 = self.extract_lga_and_year_and_disease_name(df,lga,2012,disease_name)
     
        #plot_std_graph_smoothed_2017(agege_malaria_df,agege_malaria_df_2017.iloc[0:12],'CO_12to59M','CO_Total',1.0)
        lower_list,upper_list = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
        #extract_year_and_normalised_2017(add_one_2017(df_2017,age_grp,grp_total),year,age_grp)
        malaria_agege_2012_CI_12to59M_2017 = self.extract_year_and_normalised_2017(self.add_one_2017(agege_malaria_df_2017.iloc[0:12],age_grp,grp_total),target_year,age_grp)
        values = malaria_agege_2012_CI_12to59M_2017[0:12]
        #print(values)
        x = self.get_months_of_the_year(df,lga)
        #print(x)
        xpos = self.make_month_dict()[list(set(df_2017.Month))[0]]
        b_list = self.get_breach_list(values,lower_list,upper_list)
        return lga, disease_name_2017, age_grp ,target_year,len(b_list)
    
    #def plot_one_month_with_alerts_2018


    def get_cx_total(self,df,lga,cx_total,disease_name,year):
        temp = self.extract_lga_and_year_and_disease_name(df,lga,year,disease_name)
        #return temp[['Reporting Month ','Year ','DiseaseType',cx_total]]
        return (temp[cx_total].values +1) #.rolling(window=3,min_periods=1).mean().values 
    
    #extract_lga_and_disease
    def get_cx_total_2017(self,df,lga,cx_total,disease_name,year):
        temp = self.extract_lga_and_disease_and_year_2017(df,lga,disease_name,year)
        #temp = extract_lga_and_year_and_disease_name_2017(df,lga,year,disease_name)
        #return temp[['Reporting Month ','Year ','DiseaseType',cx_total]]
        return (temp[cx_total].values+1) #.rolling(window=3,min_periods=1).mean().values

    #def get_cx_total_2018
    def get_cx_total_2018(self,df,lga,cx_total,disease_name,year):
        temp = self.extract_lga_and_disease_and_year_2017(df,lga,disease_name,year)
        #print(temp)
        #temp = extract_lga_and_year_and_disease_name_2017(df,lga,year,disease_name)
        #return temp[['Reporting Month ','Year ','DiseaseType',cx_total]]
        return (temp[cx_total].values+1) #.rolling(window=3,min_periods=1).mean().values

    def get_cx_total_2018_alerts(self,df,lga,cx_total,disease_name,year):
        temp = self.extract_lga_and_disease_and_year_2017(df,lga,disease_name,year)
        #print(temp)
        #temp = extract_lga_and_year_and_disease_name_2017(df,lga,year,disease_name)
        #return temp[['Reporting Month ','Year ','DiseaseType',cx_total]]
        return (temp[cx_total].values+1) #.rolling(window=3,min_periods=1).mean().values
    
    
    def get_cx_total_totals(self,df,lga,cx_total,disease_names_list,year):
        disease_cx_totals_dict = {}
        temp = [(disease_name, self.get_cx_total(df,lga,cx_total,disease_name,year)) for disease_name in disease_names_list]
        return dict(temp)
        #for disease_name in disease_names_list:
            #disease_cx_totals_dict[disease_name] = self.get_cx_total(df,lga,cx_total,disease_name,year)
        #return disease_cx_totals_dict    
    
    def get_cx_total_totals_2017(self,df,lga,cx_total,disease_names_list,year):
        disease_cx_totals_dict = {}
        temp = [(disease_name,self.get_cx_total_2017(df,lga,cx_total,disease_name,year)) for disease_name in disease_names_list]
        return dict(temp)
        #for disease_name in disease_names_list:
            #disease_cx_totals_dict[disease_name] = self.get_cx_total_2017(df,lga,cx_total,disease_name,year)
            #print(disease_cx_totals_dict[disease_name])
        #return disease_cx_totals_dict 

       
    def get_cx_total_totals_2018_alerts(self,df,lga,cx_total,disease_names_list,year):
        disease_cx_totals_dict = {}
        temp = [(disease_name,self.get_cx_total_2018(df,lga,cx_total,disease_name,year)) for disease_name in disease_names_list]
        return dict(temp)
        #for disease_name in disease_names_list:
            #disease_cx_totals_dict[disease_name] = self.get_cx_total_2018(df,lga,cx_total,disease_name,year)
            #print(disease_cx_totals_dict[disease_name])
        #return disease_cx_totals_dict     
    
    
    def get_cx_total_totals_2018(self,df,lga,cx_total,disease_names_list,year):
        disease_cx_totals_dict = {}
        temp = [(disease_name,self.get_cx_total_2018(df,lga,cx_total,disease_name,year)) for disease_name in disease_names_list]
        return dict(temp)
        #for disease_name in disease_names_list:
            #disease_cx_totals_dict[disease_name] = self.get_cx_total_2018(df,lga,cx_total,disease_name,year)
            #print(disease_cx_totals_dict[disease_name])
        #return disease_cx_totals_dict 
    
    def get_list_of_totals(self,df,lga,cx_total,disease_names_list,year):
        dict_totals = self.get_cx_total_totals(df,lga,cx_total,disease_names_list,year)
        totals_list = []
        lassa = '10.  Lassa fever (Viral hemorrhagic fever)'
        measles = '14. Measles'
        chorela = '3. Cholera'
        csm = '2.   CSM'
        diarrhoea = '5.   Diarrhoea (Watery without blood)'
        diarrhoea_with_blood = '6.   Diarrhoea (with blood)'
        malaria = '13a) Malaria '
        malaria_severe = '13b) Malaria (severe)'
        malaria_preg_women = '13c) Malaria (Pregnant Women)'
        pneumonia = '20. Pneumonia'
        disease_names_list = ['10.  Lassa fever (Viral hemorrhagic fever)',
                '14. Measles','3. Cholera','2.   CSM','5.   Diarrhoea (Watery without blood)',
                '6.   Diarrhoea (with blood)','13a) Malaria ','13b) Malaria (severe)',
                '13c) Malaria (Pregnant Women)','20. Pneumonia']
        
        totals_list = [float(dict_totals[lassa][i])+float(dict_totals[measles][i])+float(dict_totals[chorela][i])+float(dict_totals[csm][i])+float(dict_totals[diarrhoea][i])+float(dict_totals[diarrhoea_with_blood][i])+float(dict_totals[malaria][i])+float(dict_totals[malaria_severe][i])+float(dict_totals[malaria_preg_women][i])+float(dict_totals[pneumonia][i]) for i in range(12)]
        
#float(dict_totals[lassa][i])+float(dict_totals[measles][i])+float(dict_totals[chorela][i])+float(dict_totals[csm][i])+float(dict_totals[diarrhoea][i])+float(dict_totals[diarrhoea_with_blood][i])+float(dict_totals[malaria][i])+float(dict_totals[malaria_severe][i])+float(dict_totals[malaria_preg_women][i])+float(dict_totals[pneumonia][i])
        
        #for i in range(12):
            #total = 0
            #total = total + float(dict_totals[lassa][i])
            #total = total + float(dict_totals[measles][i])
            #total = total + float(dict_totals[chorela][i])
            #total = total + float(dict_totals[csm][i])
            #total = total + float(dict_totals[diarrhoea][i])
            #total = total + float(dict_totals[diarrhoea_with_blood][i])
            #total += float(dict_totals[malaria][i])
            #total += float(dict_totals[malaria_severe][i])
            #total += float(dict_totals[malaria_preg_women][i])
            #total += float(dict_totals[pneumonia][i])
            #totals_list.append(total)
        return totals_list       
    
    
    def get_list_of_totals_2018(self,df,lga,cx_total,disease_names_list,year):
        dict_totals = self.get_cx_total_totals_2018(df,lga,cx_total,disease_names_list,year)
        ############   çhanged  #########
        #get_cx_total_totals_2018_alerts
        #print('xxxxxxxx  Alerts  xxxxxxxxx')
        #print(dict_totals)
        totals_list = []
        csm_2017 = 'CSM'
        chorela_2017 = 'Cholera'
        diarrhoea_with_blood_2017 = 'Diarrhoea (with blood)'
        diarrhoea_2017 = 'Diarrhoea with dehydration(5yrs)'
        lassa_2017 = 'Lassa fever (Viral hemorrhagic fever)'
        malaria_2017 = 'Malaria'
        malaria_preg_women_2017 =  'Malaria (Pregnant Women)'
        malaria_severe_2017 = 'Malaria (severe)'
        measles_2017 ='Measles'
        pneumonia_2017 = 'Pneumonia ? 5yrs'
        disease_names_list_2018 = ['CSM', 'Cholera','Diarrhoea (with blood)','Diarrhoea with dehydration(5yrs)',
                                 'Lassa fever (Viral hemorrhagic fever)',
                                 'Malaria','Malaria (Pregnant Women)','Malaria (severe)','Measles','Pneumonia ? 5yrs']    
              
        totals_list=[float(dict_totals[lassa_2017][i])+float(dict_totals[measles_2017][i])+ float(dict_totals[chorela_2017][i]) + float(dict_totals[csm_2017][i])+float(dict_totals[diarrhoea_2017][i])+float(dict_totals[diarrhoea_with_blood_2017][i])+ float(dict_totals[malaria_2017][i])+float(dict_totals[malaria_severe_2017][i])+float(dict_totals[malaria_preg_women_2017][i])+float(dict_totals[pneumonia_2017][i])  for i in range(1)]
       
        
        
        #for i in range(12): # what should this be?
            #print(dict_totals[lassa_2017])
            #total = 0
            #total = total + float(dict_totals[lassa_2017][i])
            #total = total + float(dict_totals[measles_2017][i])
            #total = total + float(dict_totals[chorela_2017][i])
            #total = total + float(dict_totals[csm_2017][i])
            #total = total + float(dict_totals[diarrhoea_2017][i])
            #total = total + float(dict_totals[diarrhoea_with_blood_2017][i])
            #total += float(dict_totals[malaria_2017][i])
            #total += float(dict_totals[malaria_severe_2017][i])
            #total += float(dict_totals[malaria_preg_women_2017][i])
            #total += float(dict_totals[pneumonia_2017][i])
            #totals_list.append(total)
        return totals_list          

    #get_cx_total_ratios_2018_for_alerts    
    def get_list_of_totals_2018_for_alerts(self,df,lga,cx_total,disease_names_list,year):
        dict_totals = self.get_cx_total_totals_2018_alerts(df,lga,cx_total,disease_names_list,year)
        totals_list = []
        csm_2017 = 'CSM'
        chorela_2017 = 'Cholera'
        diarrhoea_with_blood_2017 = 'Diarrhoea (with blood)'
        diarrhoea_2017 = 'Diarrhoea with dehydration(5yrs)'
        lassa_2017 = 'Lassa fever (Viral hemorrhagic fever)'
        malaria_2017 = 'Malaria'
        malaria_preg_women_2017 =  'Malaria (Pregnant Women)'
        malaria_severe_2017 = 'Malaria (severe)'
        measles_2017 ='Measles'
        pneumonia_2017 = 'Pneumonia ? 5yrs'
        disease_names_list_2018 = ['CSM', 'Cholera','Diarrhoea (with blood)','Diarrhoea with dehydration(5yrs)',
                                 'Lassa fever (Viral hemorrhagic fever)',
                                 'Malaria','Malaria (Pregnant Women)','Malaria (severe)','Measles','Pneumonia ? 5yrs']    
        
            
        totals_list=[float(dict_totals[lassa_2017][i])+float(dict_totals[measles_2017][i])+ float(dict_totals[chorela_2017][i]) + float(dict_totals[csm_2017][i])+float(dict_totals[diarrhoea_2017][i])+float(dict_totals[diarrhoea_with_blood_2017][i])+ float(dict_totals[malaria_2017][i])+float(dict_totals[malaria_severe_2017][i])+float(dict_totals[malaria_preg_women_2017][i])+float(dict_totals[pneumonia_2017][i])  for i in range(1)]
      
        
        
        #print(dict_totals)
        #for i in range(1): # what should this be?
            
            #total = 0
            #total = total + float(dict_totals[lassa_2017][i])
           # total = total + float(dict_totals[measles_2017][i])
            #total = total + float(dict_totals[chorela_2017][i])
            #total = total + float(dict_totals[csm_2017][i])
            #total = total + float(dict_totals[diarrhoea_2017][i])
            #total = total + float(dict_totals[diarrhoea_with_blood_2017][i])
            #total += float(dict_totals[malaria_2017][i])
            #total += float(dict_totals[malaria_severe_2017][i])
            #total += float(dict_totals[malaria_preg_women_2017][i])
            #total += float(dict_totals[pneumonia_2017][i])
            #totals_list.append(total)
        return totals_list          
             
        
        
    #'Diarrhoea with dehydration (< 5yrs)'    
    #def get_list_of_totals_2017
    def get_list_of_totals_2017(self,df,lga,cx_total,disease_names_list,year):
        dict_totals = self.get_cx_total_totals_2017(df,lga,cx_total,disease_names_list,year)
        totals_list = []
        csm_2017 = 'CSM'
        chorela_2017 = 'Cholera'
        diarrhoea_with_blood_2017 = 'Diarrhoea (with blood)'
        #diarrhoea_2017 = 'Diarrhoea with dehydration (< 5yrs)'
        lassa_2017 = 'Lassa fever (Viral hemorrhagic fever)'
        malaria_2017 = 'Malaria '
        malaria_preg_women_2017 =  'Malaria (Pregnant Women)'
        malaria_severe_2017 = 'Malaria (severe)'
        measles_2017 ='Measles'
        pneumonia_2017 = 'Pneumonia (< 5yrs)'
        disease_names_list = ['CSM','Cholera','Diarrhoea (with blood)',
                'Diarrhoea with dehydration (< 5yrs)','Lassa fever (Viral hemorrhagic fever)',
             'Malaria ','Malaria (Pregnant Women)','Malaria (severe)','Measles','Pneumonia (< 5yrs)']
        
        
        totals_list=[float(dict_totals[lassa_2017][i])+float(dict_totals[measles_2017][i])+ float(dict_totals[chorela_2017][i]) + float(dict_totals[csm_2017][i])+float(dict_totals[diarrhoea_2017][i])+float(dict_totals[diarrhoea_with_blood_2017][i])+ float(dict_totals[malaria_2017][i])+float(dict_totals[malaria_severe_2017][i])+float(dict_totals[malaria_preg_women_2017][i])+float(dict_totals[pneumonia_2017][i])  for i in range(12)]
        
        #float(dict_totals[lassa_2017][i])+float(dict_totals[measles_2017][i])+ float(dict_totals[chorela_2017][i]) + float(dict_totals[csm_2017][i])+float(dict_totals[diarrhoea_2017][i])+float(dict_totals[diarrhoea_with_blood_2017][i])+ float(dict_totals[malaria_2017][i])+float(dict_totals[malaria_severe_2017][i])+float(dict_totals[malaria_preg_women_2017][i])+float(dict_totals[pneumonia_2017][i])
        
        
        
        
        
        #for i in range(12):
            #total = 0
            #total = total + float(dict_totals[lassa_2017][i])
            #total = total + float(dict_totals[measles_2017][i])
            #total = total + float(dict_totals[chorela_2017][i])
            #total = total + float(dict_totals[csm_2017][i])
            #total = total + float(dict_totals[diarrhoea_2017][i])
            #total = total + float(dict_totals[diarrhoea_with_blood_2017][i])
            #total += float(dict_totals[malaria_2017][i])
            #total += float(dict_totals[malaria_severe_2017][i])
            #total += float(dict_totals[malaria_preg_women_2017][i])
            #total += float(dict_totals[pneumonia_2017][i])
            #totals_list.append(total)
        return totals_list  
    
    def get_cx_total_ratios(self,df,lga,cx_total,target_disease_name,disease_names_list,year):    
        dict_totals = self.get_list_of_totals(df,lga,cx_total,disease_names_list,year)
        agege_2012 = self.get_cx_total(df,lga,cx_total,target_disease_name,year)
        return agege_2012/dict_totals   
######## get_cx_total_ratios_2018
    
    def get_smoothed_ratio(self,r1):

        return pd.Series(data=r1).rolling(window=3,min_periods=1).mean()
      
    
    def calculate_average_and_std(self,df12, df13, df14, df15):
        #mean_list = [0 for i in range(len(df12))]
        #std_list = [0 for i in range(len(df12))]
        mean_list =[statistics.mean([df12[i],df13[i],df14[i],df15[i]]) for i in range(len(df12))]
        std_list = [statistics.stdev([df12[i],df13[i],df14[i],df15[i]]) for i in range(len(df12))]
        #for i in range(len(df12)):
            #mean_list[i] = statistics.mean([df12[i],df13[i],df14[i],df15[i]])
            #std_list[i] = statistics.stdev([df12[i],df13[i],df14[i],df15[i]])
        return np.array(mean_list), np.array(std_list)    
    
    def get_cx_total_2017(self,df,lga,cx_total,disease_name,year):
        temp = self.extract_lga_and_disease_2017(df,lga,disease_name)
        #return temp[['Reporting Month ','Year ','DiseaseType',cx_total]]
        return temp[cx_total].values #.rolling(window=3,min_periods=1).mean().values   
 
    #self.get_cx_total_ratios_2018
    #def get_cx_total_ratios_2017(self,df,lga,cx_total,target_disease_name,disease_names_list,year):
    def get_cx_total_ratios_2017(self,df,lga,cx_total,target_disease_name,disease_names_list,year):    
        dict_totals = self.get_list_of_totals_2017(df,lga,cx_total,disease_names_list,year)
        agege_2012 = self.get_cx_total_2017(df,lga,cx_total,target_disease_name,year)
        return agege_2012[0:12]/dict_totals 
   
    
    #def get_cx_total_ratios_2018
    #def get_cx_total_ratios_2017(self,df,lga,cx_total,target_disease_name,disease_names_list,year):
    def get_cx_total_ratios_2018(self,df,lga,cx_total,target_disease_name,disease_names_list,year):    
        dict_totals = self.get_list_of_totals_2018(df,lga,cx_total,disease_names_list,year)
        #print(dict_totals)
        agege_2012 = self.get_cx_total_2018(df,lga,cx_total,target_disease_name,year)
        return agege_2012[0:12]/dict_totals 
    
    
    def get_cx_total_ratios_2018_alerts(self,df,lga,cx_total,target_disease_name,disease_names_list,year):    
        dict_totals = self.get_list_of_totals_2018_for_alerts(df,lga,cx_total,disease_names_list,year)
        agege_2012 = self.get_cx_total_2018_alerts(df,lga,cx_total,target_disease_name,year)
        return agege_2012[0:12]/dict_totals      

 
    ###********def count_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts**************
    
    def calculate_red_bounds(self,df,lga,cx_total,target_disease,taget_disease_2017,disease_names_list,disease_names_list_2017):
        #yr_2012 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2012,age_grp)
        #yr_2013 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2013,age_grp)
        #yr_2014 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2014,age_grp) 
        #yr_2015 = self.extract_year_and_normalised(self.add_one_with_smoothing(df,age_grp,grp_total),2015,age_grp)

        #model_mean, model_std = self.calculate_average_and_std(yr_2012,yr_2013,yr_2014,yr_2015)

        #return model_mean, model_std   
        d_2012 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2012)
        d_2012_sm = self.get_smoothed_ratio(d_2012)
        d_2013 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2013)
        d_2013_sm = self.get_smoothed_ratio(d_2013)
        d_2014 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2014)
        d_2014_sm = self.get_smoothed_ratio(d_2014)
        d_2015 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2015)
        d_2015_sm = self.get_smoothed_ratio(d_2015)
        cx_total_mean_sm, cx_tota_std_sm = self.calculate_average_and_std(d_2012_sm,d_2013_sm,d_2014_sm, d_2015_sm)    
        return cx_total_mean_sm, cx_tota_std_sm
    
    def calculate_red_model_bounds(self,df,lga):
        
        
        #lower_list,upper_list = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
        age_grp_tuple_list = ['CIO_Total','CI_Total','CO_Total']
                

        dict_of_models= {}
        for k,v in self.disease_tuple_list.items():
            #agege_malaria_df = self.extract_lga_and_disease(df,lga,v
            dict_of_models[k] = [(j,self.calculate_red_bounds(df,lga,j,v,k,self.disease_names_list,self.disease_names_list_2017)) for j in age_grp_tuple_list]                                                
            #dict_of_models[k] = [(j[1],self.calculate_yellow_bounds(agege_malaria_df,j[1],j[0])) for j in age_grp_tuple_list]
            #bounds_tuple = self.calculate_bounds(agege_malaria_df,age_grp,grp_total,std_factor)
            #dict_of_models[k] = (bounds_tuple[0], bounds_tuple[1])
            #print(bounds_tuple)

        return dict_of_models    
    
    
    def count_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts(self,df,df_2017,lga,cx_total,target_disease,taget_disease_2017,disease_names_list,disease_names_list_2017,year,std_factor=1.0):

        dict_totals_2012 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2012)
        dict_totals_2012_sm = self.get_smoothed_ratio(dict_totals_2012)
        dict_totals_2013 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2013)
        dict_totals_2013_sm = self.get_smoothed_ratio(dict_totals_2013)
        dict_totals_2014 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2014)
        dict_totals_2014_sm = self.get_smoothed_ratio(dict_totals_2014)
        dict_totals_2015 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2015)
        dict_totals_2015_sm = self.get_smoothed_ratio(dict_totals_2015)
        cx_total_mean_sm, cx_tota_std_sm = self.calculate_average_and_std(dict_totals_2012_sm,dict_totals_2013_sm,dict_totals_2014_sm, dict_totals_2015_sm)
        lower_list = np.array(cx_total_mean_sm) - np.array(cx_tota_std_sm)*std_factor
        upper_list = np.array(cx_total_mean_sm) + np.array(cx_tota_std_sm)*std_factor

        ######
        ratios_2017 = self.get_cx_total_ratios_2018_alerts(df_2017,lga,cx_total,taget_disease_2017,disease_names_list_2017,year)
 
        values = ratios_2017

        xpos = self.make_month_dict()[list(set(df_2017.Month))[0]]

        x = self.get_months_of_the_year(df,lga)
    
        #print(x)
        month = list(set(df_2017.Month))[0]
        xpos = self.make_month_dict()[list(set(df_2017.Month))[0]]
        b_list = self.get_breach_list(values,lower_list,upper_list)
        
        return lga,taget_disease_2017, month, year, len(b_list)
    
    #def plot_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts

        
    #def plot_smoothed_bounds_cx_ratios_2018_unsmoothed_many_alerts
    #count_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts
    def count_smoothed_bounds_cx_ratios_2018_unsmoothed_many_alerts(self,df,df_2017,lga,cx_total,target_disease,taget_disease_2017,disease_names_list,disease_names_list_2017,year,std_factor=1.0):

        dict_totals_2012 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2012)
        dict_totals_2012_sm = self.get_smoothed_ratio(dict_totals_2012)
        #print(dict_totals_2012_sm)
        dict_totals_2013 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2013)
        dict_totals_2013_sm = self.get_smoothed_ratio(dict_totals_2013)
        #print(dict_totals_2013_sm)
        dict_totals_2014 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2014)
        dict_totals_2014_sm = self.get_smoothed_ratio(dict_totals_2014)
        #print(dict_totals_2014_sm)
        dict_totals_2015 = self.get_cx_total_ratios(df,lga,cx_total,target_disease,disease_names_list,2015)
        dict_totals_2015_sm = self.get_smoothed_ratio(dict_totals_2015)
        #print(dict_totals_2015_sm)
        cx_total_mean_sm, cx_tota_std_sm = self.calculate_average_and_std(dict_totals_2012_sm,dict_totals_2013_sm,dict_totals_2014_sm, dict_totals_2015_sm)
        lower_bound = np.array(cx_total_mean_sm) - np.array(cx_tota_std_sm)*std_factor
        upper_bound = np.array(cx_total_mean_sm) + np.array(cx_tota_std_sm)*std_factor

        ratios_2017 = self.get_cx_total_ratios_2018(df_2017,lga,cx_total,taget_disease_2017,disease_names_list_2017,year)
        #print(ratios_2017)
        values = ratios_2017
        x = self.get_months_of_the_year(df,lga)
        month = list(set(df_2017.Month))[0]
        xpos = self.make_month_dict()[month]
        
        b_list = self.get_breach_list(values,lower_bound,upper_bound)    
        return lga,target_disease, month, year, len(b_list) 
    
      
    #def .plot_smoothed_bounds_cx_ratios_2017_unsmoothed 
    #def plot_smoothed_bounds_cx_ratios_2017_unsmoothed
    #def plot_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts
    #plot_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts
    
    def make_month_dict(self):
        month_dict = {}
        month_dict['January'] = 0
        month_dict['February'] = 1
        month_dict['March'] = 2
        month_dict['April'] = 3
        month_dict['May'] = 4
        month_dict['June'] = 5
        month_dict['July'] = 6
        month_dict['August'] = 7
        month_dict['September'] = 8
        month_dict['October'] = 9
        month_dict['November'] = 10
        month_dict['December'] = 11
        month_dict['Jan']  = 0
        month_dict['Feb']  = 1
        month_dict['Mar']  = 2
        month_dict['Apr']  = 3
        month_dict['May']  = 4
        month_dict['Jun']  = 5
        month_dict['Jul']  = 6
        month_dict['Aug']  = 7
        month_dict['Sep']  = 8
        month_dict['Oct']  = 9
        month_dict['Nov']  = 10
        month_dict['Dec'] = 11
        return month_dict

    def make_abrev_month_dict(self):
        month_dict = {}
        month_dict['Jan'] = 0
        month_dict['Feb'] = 1
        month_dict['Mar'] = 2
        month_dict['Apr'] = 3
        month_dict['May'] = 4
        month_dict['Jun'] = 5
        month_dict['Jul'] = 6
        month_dict['Aug'] = 7
        month_dict['Sep'] = 8
        month_dict['Oct'] = 9
        month_dict['Nov'] = 10
        month_dict['Dec'] = 11
        return month_dict

    def combine_df(self,df_list12):
        df_combined12 = pd.concat(df_list12)
        #print(df_combined12)
        return df_combined12
    
    
    def get_annual_red_alerts(self,df_2012_16,df_test_all_2018,lga,cx_total,disease_names_list,disease_names_list_2018,year,std_factor):
           
        ##
        ## Yearly Red Alerts
        ##
        results = [alerts.count_smoothed_bounds_cx_ratios_2018_unsmoothed_many_alerts(df_2012_16,df_test_all_2018,lga,cx_total,self.disease_tuple_list[i],i,disease_names_list,disease_names_list_2018,year,std_factor) for i in self.disease_tuple_list]
        #for i in self.disease_tuple_list:
            #results.append(alerts.count_smoothed_bounds_cx_ratios_2018_unsmoothed_many_alerts(df_2012_16,df_test_all_2018,lga,cx_total,self.disease_tuple_list[i],i,disease_names_list,disease_names_list_2018,year,std_factor))

        return results

###### 

    def get_monthly_yellow_alert(self,df_2012_16,df_test_apr_2018,lga,year,std_factor = 1.5):
        
        ##
        ## Monthly  Yellow Alert
        ##
        month = list(set(df_test_apr_2018.Month))[0]
        year = list(set(df_test_apr_2018.Year))[0]
        age_grp_dict = {}
        results_list = []
        disease_dict_totals = {}
        age_grp_tuple_list = [('CI_Total','CI_0to28D'),('CI_Total','CI_1to11M'), ('CI_Total','CI_12to59M'), ('CI_Total','CI_5to9Y'), ('CI_Total','CI_10to19Y'), ('CI_Total','CI_20to40Y'),('CI_Total','CI_Ovr40Y'),
                            ('CO_Total','CO_0to28D'),('CO_Total','CO_1to11M'), ('CO_Total','CO_12to59M'), ('CO_Total','CO_5to9Y'), ('CO_Total','CO_10to19Y'), ('CO_Total','CO_20to40Y'),('CO_Total','CO_Ovr40Y')]
        for i in self.disease_tuple_list:
            
            temp = [self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,j[1],j[0],year,std_factor) for j in age_grp_tuple_list]
                #result = self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,j[1],j[0],year,std_factor)
            #total = [self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,j[1],j[0],year,std_factor) for j in age_grp_tuple_list]]
            
            total = sum([k[4] for k in temp])    
                #total = total + result[4]
                #print(total)
            disease_dict_totals[i] = lga,i,month,year,total
            
        #total = 0
        #for i in self.disease_tuple_list:
            #disease_dict_totals[i] = 0
            #total = 0
            #for key,value in age_grp_dict.items():
                #disease_dict_totals[i] = total
                #for v in value:
                    #result = self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,v,key,year,std_factor)
                    #total = total + result[4]
                    
            #disease_dict_totals[i] = lga,i,month,year,total

        results_list = [tuple(v) for k,v in disease_dict_totals.items()]    
        #for k,v in disease_dict_totals.items():
            #results_list.append(tuple(v))
        return results_list
    
######
#*******
    def xxxxget_monthly_yellow_alert(self,df_2012_16,df_test_apr_2018,lga,year,std_factor = 1.5):
        
        ##
        ## Monthly  Yellow Alert
        ##
        month = list(set(df_test_apr_2018.Month))[0]
        year = list(set(df_test_apr_2018.Year))[0]
        age_grp_dict = {}
        results_list = []
        disease_dict_totals = {}
        age_grp_dict['CI_Total'] = ['CI_0to28D','CI_1to11M', 'CI_12to59M', 'CI_5to9Y', 'CI_10to19Y', 'CI_20to40Y','CI_Ovr40Y']
        age_grp_dict['CO_Total'] = ['CO_0to28D','CO_1to11M', 'CO_12to59M', 'CO_5to9Y', 'CO_10to19Y', 'CO_20to40Y','CO_Ovr40Y']
        total = 0
        for i in self.disease_tuple_list:
            disease_dict_totals[i] = 0
            total = 0
            for key,value in age_grp_dict.items():
                disease_dict_totals[i] = total
                for v in value:
                    result = self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,v,key,year,std_factor)
                    total = total + result[4]
                    
            disease_dict_totals[i] = lga,i,month,year,total

        results_list = [tuple(v) for k,v in disease_dict_totals.items()]    
        #for k,v in disease_dict_totals.items():
            #results_list.append(tuple(v))
        return results_list



#*******

        
    
    def get_annual_yellow_alert(self,df_2012_16,df_test_apr_2018,lga,year,std_factor = 1.5):
        
        ##
        ## Annual  Yellow Alert
        ##

        age_grp_dict = {}
        disease_dict_totals = {}
        age_grp_dict['CI_Total'] = ['CI_0to28D','CI_1to11M', 'CI_12to59M', 'CI_5to9Y', 'CI_10to19Y', 'CI_20to40Y','CI_Ovr40Y']
        age_grp_dict['CO_Total'] = ['CO_0to28D','CO_1to11M', 'CO_12to59M', 'CO_5to9Y', 'CO_10to19Y', 'CO_20to40Y','CO_Ovr40Y']
        total = 0
        for i in self.disease_tuple_list:
            disease_dict_totals[i] = 0
            total = total
            for key,value in age_grp_dict.items():
                disease_dict_totals[i] = total
                for v in value:
                    result = self.count_one_month_with_alerts_2018(df_2012_16,df_test_apr_2018,lga,self.disease_tuple_list[i],i,v,key,year,std_factor)
                    total = total + result[4]
            #print(result)
        #print('XXXXXXXXXXXXXXXXXXXXXXX')
        return disease_dict_totals    
    
        #get_monthly_red_alerts
    
    def get_monthly_red_alerts(self,df_2012_16, df_bad_apr_2018,lga,cxx_total,disease_names_list,disease_names_list_2018,year,std_factor = 1.5):
        ##
        ##    Monthly Red Alerts
        ##
        results = [self.count_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts(df_2012_16,df_bad_apr_2018,lga,'CIO_Total',self.disease_tuple_list[i],i,self.disease_names_list,self.disease_names_list_2018,2018,1.5) for i in self.disease_tuple_list]
        #for i in self.disease_tuple_list:

            #results.append(self.count_smoothed_bounds_cx_ratios_2017_unsmoothed_alerts(df_2012_16,df_bad_apr_2018,lga,'CIO_Total',self.disease_tuple_list[i],i,self.disease_names_list,self.disease_names_list_2018,2018,1.5))
    
        return results



