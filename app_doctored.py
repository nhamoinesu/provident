# Flask Packages
from flask import Flask,render_template,request,url_for
from flask_bootstrap import Bootstrap 
from flask_uploads import UploadSet,configure_uploads,IMAGES,DATA,ALL
from flask_sqlalchemy import SQLAlchemy 

from werkzeug import secure_filename
import os
import datetime
import time
#
#import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
#from sklearn.metrics import mean_squared_error
#from sklearn.preprocessing import MinMaxScaler
#from keras.models import Sequential
#from keras.layers import Dense
#from keras.layers import LSTM
from math import sqrt
from matplotlib import pyplot
# load and plot dataset
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
import time
import datetime
import numpy as np
#from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics
import matplotlib.pyplot as plt

# EDA Packages
import pandas as pd 
import numpy as np 

# ML Packages
#from sklearn import model_selection
#from sklearn.linear_model import LogisticRegression
#from sklearn.tree import DecisionTreeClassifier
#from sklearn.neighbors import KNeighborsClassifier
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
#from sklearn.naive_bayes import GaussianNB
#from sklearn.svm import SVC


# ML Packages For Vectorization of Text For Feature Extraction
#from sklearn.feature_extraction.text import CountVectorizer
#from sklearn.feature_extraction.text import TfidfVectorizer

from AlertsGenerator import AlertsGenerator
from ExcelFilesReader import ExcelFilesReader

alerts = AlertsGenerator('out_all_files_except_2016b_and_2017.csv','out_2017.csv')
f_reader = ExcelFilesReader()

app = Flask(__name__)
Bootstrap(app)
db = SQLAlchemy(app)

# Configuration for File Uploads
files = UploadSet('files',ALL)
app.config['UPLOADED_FILES_DEST'] = 'static/uploadsDB'
configure_uploads(app,files)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///static/uploadsDB/filestorage.db'
base_path = os.path.dirname(__file__)

# Saving Data To Database Storage
class FileContents(db.Model):
	id = db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(300))
	modeldata = db.Column(db.String(300))
	data = db.Column(db.LargeBinary)

@app.route('/')
def index():
	print(base_path)
	return render_template('index.html')

# Route for our Processing and Details Page
@app.route('/dataupload',methods=['GET','POST'])
def dataupload():
	if request.method == 'POST' and 'xls_data' in request.files:
		file = request.files['xls_data']
		filename = secure_filename(file.filename)
		uploads_folder = os.path.join(base_path, "static", "uploadsDB")
		# # os.path.join is used so that paths work in every operating system
		# # file.save(os.path.join("wherever","you","want",filename))
		file.save(os.path.join(uploads_folder,filename))
		fullfile = os.path.join(uploads_folder,filename)
		dataset_check = pd.read_excel(fullfile)
		fullfile1 = dataset_check.head()
		# # For Time
		date = str(datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S"))

		# # Exploratory Data Analysis function
		df = pd.read_excel(fullfile)
		df_size = df.size
		df_shape = df.shape
		df_columns = list(df.columns)
		df_targetname = df[df.columns[-1]].name
		df_featurenames = df_columns[0:-1] # select all columns till last column
		df_Xfeatures = df.iloc[:,0:-1] 
		df_Ylabels = df[df.columns[-1]] # Select the last column as target
		#=======
	
	red_month = report_monthly_red(fullfile)
		
	# In[7]:
	#yellow =[]
	yellow = (report_monthly_yellow(fullfile))
	yellow = str(yellow).split(',')
	yellow_month = yellow
	return render_template('details.html',filename=fullfile,date=date,
		df_size=df_size,
		df_shape=df_shape,
		df_columns =df_columns,
		df_targetname =df_targetname,
		fullfile = fullfile,
		dfplot = df.head(),
		red_month = red_month,
		yellow_month = yellow_month,
		df_featurenames = df_featurenames
		)

	
	
	

if __name__ == '__main__':
	app.run(debug=True)
