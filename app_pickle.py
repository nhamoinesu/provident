# Flask Packages
from flask import Flask,render_template,request,url_for
from flask_bootstrap import Bootstrap 
from flask_uploads import UploadSet,configure_uploads,IMAGES,DATA,ALL
from flask_sqlalchemy import SQLAlchemy 

from werkzeug import secure_filename
import os
import datetime
import time
#
#import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
#from sklearn.metrics import mean_squared_error
#from sklearn.preprocessing import MinMaxScaler
#from keras.models import Sequential
#from keras.layers import Dense
#from keras.layers import LSTM
from math import sqrt
from matplotlib import pyplot
# load and plot dataset
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
import time
import datetime
import numpy as np
#from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics
import matplotlib.pyplot as plt

# EDA Packages
import pandas as pd 
import numpy as np 
import pickle

# ML Packages
#from sklearn import model_selection
#from sklearn.linear_model import LogisticRegression
#from sklearn.tree import DecisionTreeClassifier
#from sklearn.neighbors import KNeighborsClassifier
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
#from sklearn.naive_bayes import GaussianNB
#from sklearn.svm import SVC


# ML Packages For Vectorization of Text For Feature Extraction
#from sklearn.feature_extraction.text import CountVectorizer
#from sklearn.feature_extraction.text import TfidfVectorizer

from AlertsGenerator import AlertsGenerator
from ExcelFilesReader import ExcelFilesReader

alerts = AlertsGenerator('out_all_files_except_2016b_and_2017.csv','out_2017.csv')
f_reader = ExcelFilesReader()

pickle_in = open("yellow_model.pickle","rb")
yellow_dict = pickle.load(pickle_in)


pickle_in = open("red_model.pickle","rb")
red_dict = pickle.load(pickle_in)

app = Flask(__name__)
Bootstrap(app)
db = SQLAlchemy(app)

# Configuration for File Uploads
files = UploadSet('files',ALL)
app.config['UPLOADED_FILES_DEST'] = 'static/uploadsDB'
configure_uploads(app,files)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///static/uploadsDB/filestorage.db'
base_path = os.path.dirname(__file__)

# Saving Data To Database Storage
class FileContents(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(300))
    modeldata = db.Column(db.String(300))
    data = db.Column(db.LargeBinary)

@app.route('/')
def index():
    print(base_path)
    return render_template('index.html')

# Route for our Processing and Details Page
@app.route('/dataupload',methods=['GET','POST'])
def dataupload():
    if request.method == 'POST' and 'xls_data' in request.files:
        file = request.files['xls_data']
        filename = secure_filename(file.filename)
        uploads_folder = os.path.join(base_path, "static", "uploadsDB")
        # # os.path.join is used so that paths work in every operating system
        # # file.save(os.path.join("wherever","you","want",filename))
        file.save(os.path.join(uploads_folder,filename))
        fullfile = os.path.join(uploads_folder,filename)
        dataset_check = pd.read_excel(fullfile)
        fullfile1 = dataset_check.head()
        # # For Time
        date = str(datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S"))

        # # Exploratory Data Analysis function
        df = pd.read_excel(fullfile)
        df_size = df.size
        df_shape = df.shape
        df_columns = list(df.columns)
        df_targetname = df[df.columns[-1]].name
        df_featurenames = df_columns[0:-1] # select all columns till last column
        df_Xfeatures = df.iloc[:,0:-1] 
        df_Ylabels = df[df.columns[-1]] # Select the last column as target
        #=======
    
    red_month = report_monthly_red(fullfile,red_dict)

    # In[7]:
    #yellow =[]
    yellow = report_monthly_yellow(fullfile,yellow_dict)
    #yellow = str(yellow).split(',')
    yellow_month = yellow
    return render_template('details.html',filename=fullfile,date=date,
        df_size=df_size,
        df_shape=df_shape,
        df_columns =df_columns,
        df_targetname =df_targetname,
        fullfile = fullfile,
        dfplot = df.head(),
        red_month = red_month,
        yellow_month = yellow_month,
        df_featurenames = df_featurenames
        )


def create_badagry_df():

    month_list = ['LGA_Badagry_IDSR_003_January2018.xlsx',
            'LGA_Badagry_IDSR_003_February2018.xlsx',    
    'LGA_Badagry_IDSR_003_March2018.xlsx',
    'LGA_Badagry_IDSR_003_April2018.xlsx',
    'LGA_Badagry_IDSR_003_May2018.xlsx',
    'LGA_Badagry_IDSR_003_June2018.xlsx',
    'LGA_Badagry_IDSR_003_July2018.xlsx',              
    'LGA_Badagry_IDSR_003_August2018.xlsx',
    'LGA_Badagry_IDSR_003_September2018.xlsx',              
    'LGA_Badagry_IDSR_003_October2018.xlsx',
    'LGA_Badagry_IDSR_003_November2018.xlsx',
    'LGA_Badagry_IDSR_003_December2018.xlsx']


    df_list_18 = []
    for i in month_list:
    
        xls_18 = f_reader.read_excel_file(i)
        sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
        df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
        df_list_18.append(df_apr_18_temp)
        
    epe_all_2018_df = f_reader.combine_df(df_list_18)
    epe_all_2018_df.to_csv('out_bad_all_2018.csv', sep=',') 
    
    return alerts.read_test_data('out_bad_all_2018.csv')

def create_epe_df():
    month_list =['LGA_EPE_IDSR_003_January2018.xlsx',
        'LGA_EPE_IDSR_003_February2018.xlsx',
        'LGA_EPE_IDSR_003_March2018.xlsx',
        'LGA_EPE_IDSR_003_April2018.xlsx',
        'LGA_EPE_IDSR_003_May2018.xlsx',
        'LGA_EPE_IDSR_003_June2018.xlsx',
        'LGA_EPE_IDSR_003_July2018.xlsx',
        'LGA_EPE_IDSR_003_August2018.xlsx',
        'LGA_EPE_IDSR_003_September2018.xlsx',
        'LGA_EPE_IDSR_003_October2018.xlsx',
        'LGA_EPE_IDSR_003_November2018.xlsx',
        'LGA_EPE_IDSR_003_December2018.xlsx']

    df_list_18 = []
    for i in month_list:
    
        xls_18 = f_reader.read_excel_file(i)
        sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
        df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
        df_list_18.append(df_apr_18_temp)
        
    epe_all_2018_df = f_reader.combine_df(df_list_18)
    epe_all_2018_df.to_csv('out_epe_all_2018.csv', sep=',') 
    
    return alerts.read_test_data('out_epe_all_2018.csv')

def get_epe_test_charts_data(filename,disease_name,column_name):
    df = create_epe_df()
    df = df.loc[df['Disease Name'] == disease_name]
    return df[['Month',column_name]].values
    
    
def get_badagry_test_charts_data(filename,disease_name,column_name):
    df = create_badagry_df()
    df = df.loc[df['Disease Name'] == disease_name]
    return df[['Month',column_name]].values


def parse_test_file(file):
    xls_18 = f_reader.read_excel_file(file)
    sheet_to_df_map_18 = f_reader.parse_excel_sheets(xls_18)
    df_apr_18_temp = f_reader.create_df_18(sheet_to_df_map_18)
    df_apr_18_temp.to_csv('test_file_2018.csv', sep=',')
    standard_df_test_2018 = alerts.read_test_data('test_file_2018.csv')
    return standard_df_test_2018

def report_monthly_yellow(file,yellow_dict):
    standard_df_test_2018 = parse_test_file(file)
    lga = list(set(standard_df_test_2018.LGA))[0]
    month = list(set(standard_df_test_2018.Month))[0]
    year = list(set(standard_df_test_2018.Year))[0]
    std_factor = 1.5
    #df_tuple = extract_disease_syndrome_dfs(alerts.standard_df_2012_16, standard_df_test_2018)
    #return alerts.get_monthly_yellow_alert(df_tuple[0], df_tuple[1],lga,year,std_factor)
    return alerts.get_monthly_yellow_alert(yellow_dict,standard_df_test_2018,lga,2018,1.5)
   
def report_monthly_red(file,red_dict,cx_total='CIO_Total'):
    standard_df_test_2018 = parse_test_file(file)
    lga = list(set(standard_df_test_2018.LGA))[0]
    month = list(set(standard_df_test_2018.Month))[0]
    year = list(set(standard_df_test_2018.Year))[0]
    std_factor = 1.5
    #print(lga)
    #df_tuple = extract_disease_syndrome_dfs(alerts.standard_df_2012_16, standard_df_test_2018)
    return alerts.get_monthly_red_alerts(red_dict, standard_df_test_2018,lga,alerts.disease_names_list,alerts.disease_names_list_2018,year,std_factor)
    #return alerts.get_monthly_red_alerts(df_tuple[0], df_tuple[1],lga,'CIO_Total',alerts.disease_names_list,alerts.disease_names_list_2018,year,std_factor)








if __name__ == '__main__':
    app.run(debug=True)


