import pandas as pd
import statistics
import pandas as pd
import matplotlib.pyplot as plt
from pandas import datetime
from math import sqrt
from matplotlib import pyplot
from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
import time
import datetime
import numpy as np
from math import sqrt

import statistics
class ExcelFilesReader:
    import pandas as pd
    def __init__(self):
        self.region_area_line = 9
        self.reporting_area_label = 0
        self.reporting_area=1
        self.reporting_state_label =4
        self.reporting_state=5
        self.total_hfs_label = 8
        self.total_hfs=12
        self.reporting_status_label = 14
        self.state_reporting_status=17
    #===========================
        self.reporting_time_line = 11
        self.reporting_month_label = 0
        self.reporting_month = 1
        self.reporting_year_label = 4
        self.reporting_year = 5
        self.hfs_reporting_timely_label = 8
        self.hfs_reporting_timely = 12
        self.reporting_late_label = 14
        self.hfs_reporting_late = 17 
        self.hfs_not_reporting_label = 19
        self.hfs_not_reporting = 22
    #==========================
    #Cases out-patients index 14
        self.cases_line = 14
        self.cases_out_patients = 1
        self.cases_in_patients =9
        self.total_cases_in_and_out_patient = 17
        self.deaths = 18
    #=====================
    #DISEASE index 15
        self.disease_age_codes = 15
    #DiseaseTypes index 16
        self.disease_type_codes = 16
    #========================
    #reporting LGA index 9
        self.reporting_LGA_index = 9
    #reporting Month index 11
        self.reporting_month_index = 11


        self.arr = [9,81,151,223,293,363,433,503,573,643,713,783]
        self.areas = ['LGA1','LGA2','LGA3','LGA4','LGA5','LGA6','LGA7','LGA8','LGA9','LGA10','LGA11','LGA12','LGA13','LGA14','LGA15']
    
    #@staticmethod
    def read_excel_file(self,f):
        return pd.ExcelFile(f)
        
    # Now you can list all sheets in the file
    
    
    #@staticmethod
    def parse_excel_sheets(self,xls):
        #xls.sheet_names
        # to read all sheets to a map
        sheet_to_df_map = {}
        for sheet_name in xls.sheet_names:
            sheet_to_df_map[sheet_name] = xls.parse(sheet_name)
        return sheet_to_df_map
        
    #@staticmethod    
    def get_headers(self,sheet_map):
        #assemble headers
        areas = ['LGA1','LGA2','LGA3','LGA4','LGA5','LGA6','LGA7','LGA8','LGA9','LGA10','LGA11','LGA12','LGA13','LGA14','LGA15']
        headers = []
        region_area_line = 9
        reporting_area_label = 0
        headers.append(sheet_map[areas[0]].index[region_area_line][reporting_area_label])
        reporting_state_label =4
        headers.append(sheet_map[areas[0]].index[region_area_line][reporting_state_label])
        total_hfs_label = 8
        headers.append(sheet_map[areas[0]].index[region_area_line][total_hfs_label])
        reporting_status_label = 14
        headers.append(sheet_map[areas[0]].index[region_area_line][reporting_status_label])
        state_reporting_status=17
        reporting_time_line = 11
        reporting_month_label = 0
        headers.append(sheet_map[areas[0]].index[reporting_time_line][reporting_month_label])
        reporting_year_label = 4
        headers.append(sheet_map[areas[0]].index[reporting_time_line][reporting_year_label])
        hfs_reporting_timely_label = 8
        headers.append(sheet_map[areas[0]].index[reporting_time_line][hfs_reporting_timely_label])
        reporting_late_label = 14
        headers.append(sheet_map[areas[0]].index[reporting_time_line][reporting_late_label])
        hfs_not_reporting_label = 19
        headers.append(sheet_map[areas[0]].index[reporting_time_line][hfs_not_reporting_label])
        for i in sheet_map[areas[0]].index[16]:
            headers.append(i)
            #len(headers)
        return headers
    
    #@staticmethod
    def create_df_12(self,sheet_map):
        arr = [9,81,151,223,293,363,433,503,573,643,713,783,853]
        areas = list(sheet_map.keys())[1:-4]#['LGA1','LGA2','LGA3','LGA4','LGA5','LGA6','LGA7','LGA8','LGA9','LGA10','LGA11','LGA12','LGA13','LGA14','LGA15']
        # print(areas)
        headers = self.get_headers(sheet_map)
        df_list12 = []
        #assemble fixed values
        for a in areas:
            for j in arr:
                disease_start = j+8
                fixed_values = []
                region_area_line = j
                reporting_area_label = 0
                reporting_area=1
                fixed_values.append(sheet_map[a].index[region_area_line][reporting_area])
                reporting_state_label =4
                reporting_state=5
                fixed_values.append(sheet_map[a].index[region_area_line][reporting_state])
                total_hfs_label = 8
                total_hfs=12
                fixed_values.append(sheet_map[a].index[region_area_line][total_hfs])
                reporting_status_label = 14
                state_reporting_status=17
                fixed_values.append(sheet_map[a].index[region_area_line][state_reporting_status])
                reporting_time_line = j+2

                reporting_month = 1
                fixed_values.append(sheet_map[a].index[reporting_time_line][reporting_month])

                reporting_year = 5
                fixed_values.append(sheet_map[a].index[reporting_time_line][reporting_year])

                hfs_reporting_timely = 12
                fixed_values.append(sheet_map[a].index[reporting_time_line][hfs_reporting_timely])

                hfs_reporting_late = 17 
                fixed_values.append(sheet_map[a].index[reporting_time_line][hfs_reporting_late])

                hfs_not_reporting = 22
                fixed_values.append(sheet_map[a].index[reporting_time_line][hfs_not_reporting])
                #fixed_values
                data = []
                for k in range(disease_start,disease_start+49):
                    data.append(fixed_values+list(sheet_map[a].index[k]))
                df_list12.append(pd.DataFrame(data=data, columns=headers))
        return df_list12
    
    
    #@staticmethod
    def combine_df(self,df_list12):
        df_combined12 = pd.concat(df_list12)
        #print(df_combined12)
        return df_combined12
    
    #@staticmethod
    def create_df_list_14(self,df_map):
        #arr = [9,81,151,223,293,363,433,503,573,643,713,783,853]
        #areas = ['LGA1']#,'LGA2','LGA3','LGA4','LGA5','LGA6','LGA7','LGA8','LGA9','LGA10','LGA11','LGA12','LGA13','LGA14','LGA15']

        df_list14 = []
        #assemble fixed values
        for a in areas:
            for j in arr:
                disease_start = j+8
                fixed_values = []
                region_area_line = j
                reporting_area_label = 0
                reporting_area=1
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_area])
                reporting_state_label =4
                reporting_state=5
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_state])
                total_hfs_label = 8
                total_hfs=12
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][total_hfs])
                reporting_status_label = 14
                state_reporting_status=17
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][state_reporting_status])
                reporting_time_line = j+2

                reporting_month = 1
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_month])

                reporting_year = 5
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_year])

                hfs_reporting_timely = 12
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_timely])

                hfs_reporting_late = 17 
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_late])

                hfs_not_reporting = 22
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_not_reporting])
                #fixed_values
                data = []
                for k in range(disease_start,disease_start+49):
                    data.append(fixed_values+list(sheet_to_df_map[a].index[k]))
                df_list14.append(pd.DataFrame(data=data, columns=headers))
        
        return df_list14
    
    #@staticmethod
    def create_df_list_15(self,df_map):
        df_list15 = []
        #assemble fixed values
        for a in areas:
            for j in arr:
                disease_start = j+8
                fixed_values = []
                region_area_line = j
                reporting_area_label = 0
                reporting_area=1
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_area])
                reporting_state_label =4
                reporting_state=5
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_state])
                total_hfs_label = 8
                total_hfs=12
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][total_hfs])
                reporting_status_label = 14
                state_reporting_status=17
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][state_reporting_status])
                reporting_time_line = j+2

                reporting_month = 1
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_month])

                reporting_year = 5
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_year])

                hfs_reporting_timely = 12
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_timely])

                hfs_reporting_late = 17 
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_late])

                hfs_not_reporting = 22
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_not_reporting])
                #fixed_values
                data = []
                for k in range(disease_start,disease_start+49):
                    data.append(fixed_values+list(sheet_to_df_map[a].index[k]))
                df_list15.append(pd.DataFrame(data=data, columns=headers))
        return df_list15

        
    #@staticmethod
    def create_df_list_116a(self,df_map):
        df_list16a = []
        #assemble fixed values
        for a in areas:
            for j in arr:
                disease_start = j+8
                fixed_values = []
                region_area_line = j
                reporting_area_label = 0
                reporting_area=1
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_area])
                reporting_state_label =4
                reporting_state=5
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_state])
                total_hfs_label = 8
                total_hfs=12
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][total_hfs])
                reporting_status_label = 14
                state_reporting_status=17
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][state_reporting_status])
                reporting_time_line = j+2

                reporting_month = 1
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_month])

                reporting_year = 5
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_year])

                hfs_reporting_timely = 12
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_timely])

                hfs_reporting_late = 17 
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_late])

                hfs_not_reporting = 22
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_not_reporting])
                #fixed_values
                data = []
                for k in range(disease_start,disease_start+49):
                    data.append(fixed_values+list(sheet_to_df_map[a].index[k]))
                df_list16a.append(pd.DataFrame(data=data, columns=headers))
        return df_list16a



    #@staticmethod
    def create_16_july_dec_headers(self):
        headers = []
        region_area_line = 9
        reporting_area_label = 0
        headers.append(sheet_to_df_map[self.areas[0]].index[region_area_line][reporting_area_label])
        reporting_state_label =4
        headers.append(sheet_to_df_map[areas[0]].index[region_area_line][reporting_state_label])
        total_hfs_label = 8
        headers.append(sheet_to_df_map[areas[0]].index[region_area_line][total_hfs_label])
        reporting_status_label = 14
        headers.append(sheet_to_df_map[areas[0]].index[region_area_line][reporting_status_label])
        state_reporting_status=17
        reporting_time_line = 11
        reporting_month_label = 0
        headers.append(sheet_to_df_map[areas[0]].index[reporting_time_line][reporting_month_label])
        reporting_year_label = 4
        headers.append(sheet_to_df_map[areas[0]].index[reporting_time_line][reporting_year_label])
        hfs_reporting_timely_label = 8
        headers.append(sheet_to_df_map[areas[0]].index[reporting_time_line][hfs_reporting_timely_label])
        reporting_late_label = 14
        headers.append(sheet_to_df_map[areas[0]].index[reporting_time_line][reporting_late_label])
        hfs_not_reporting_label = 19
        headers.append(sheet_to_df_map[areas[0]].index[reporting_time_line][hfs_not_reporting_label])
        for i in sheet_to_df_map[areas[0]].index[16]:
            headers.append(i)
        #len(headers)
        return headers

    #@staticmethod
    def create_df_list_116b(self):
        df_list16b = []
        #assemble fixed values
        for a in self.areas:
            for j in self.arr:
                disease_start = j+8
                fixed_values = []
                region_area_line = j
                reporting_area_label = 0
                reporting_area=1
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_area])
                reporting_state_label =4
                reporting_state=5
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][reporting_state])
                total_hfs_label = 8
                total_hfs=12
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][total_hfs])
                reporting_status_label = 14
                state_reporting_status=17
                fixed_values.append(sheet_to_df_map[a].index[region_area_line][state_reporting_status])
                reporting_time_line = j+2

                reporting_month = 1
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_month])

                reporting_year = 5
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][reporting_year])

                hfs_reporting_timely = 12
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_timely])

                hfs_reporting_late = 17 
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_reporting_late])

                hfs_not_reporting = 22
                fixed_values.append(sheet_to_df_map[a].index[reporting_time_line][hfs_not_reporting])
                #fixed_values
                data = []
                for k in range(disease_start,disease_start+49):
                    data.append(fixed_values+list(sheet_to_df_map[a].index[k]))
                #data
        return df_list16b.append(pd.DataFrame(data=data[:,0:34], columns=headers))

    
    #@staticmethod
    def make_df_map(self,lgas):
        for lga in lgas:
            for i in range(17,68+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(88,139+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(159,210+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(230,281+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(301,352+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(372,423+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(443,494+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(514,565+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(585,636+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[17].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(656,707+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(727,778+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(798,849+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
            for i in range(869,920+1):
                names = list(sheet_to_df_map[lga].iloc[i].name)
                values = list(sheet_to_df_map[lga].iloc[i].values)
                temp = values + names
                data_1_2017.append(temp)
        return data_1_2017  

    
    #@staticmethod
    def make_df_17(self,d_17):
        return pd.DataFrame(data=d_17[1:],columns=d_17[0])

    
    #@staticmethod
    def create_df_18(self,sheet_to_df_map_18):
        key = list(sheet_to_df_map_18.keys())[0]
        #data_18 = sheet_to_df_map_18[key].iloc[8:].values
        #columns_18 = sheet_to_df_map_18[key].iloc[7:8].values[0]
        #columns_18 = temp_df.loc[temp_df['LGA LEVEL'].isin(['Sr No'])].values
        #x_s = columns_18.shape[1]
        #columns_18 = columns_18.reshape(x_s,)
        #columns_18 = sheet_to_df_map_18[key].values[7,:]
        cols_vals = sheet_to_df_map_18[key].values[8:,]
        #data_18 = cols_vals[1:,:]
        temp_dict = {}
        columns_18a = ['Sr No', 'Disease Name', '0-28 Days', '1-11 Months',
            '12-59 Months', '5-9 Years', '10-19 Years', '20-40 Years', '>40 Years',
            'Total', '0-28 Days.1', '1-11 Months.1', '12-59 Months.1',
            '5-9 Years.1', '10-19 Years.1', '20-40 Years.1', '>40 Years.1',
            'Total.1', 'Total Cases In & Out Patient', '0-28 Days.2',
            '1-11 Months.2', '12-59 Months.2', '5-9 Years.2', '10-19 Years.2',
                '20-40 Years.2', '>40 Years.2', 'Total.2']
    #data_2018_df = pd.DataFrame(data=data_18,columns=columns_18)
        for i in range(0,len(columns_18a)):
            temp_dict[columns_18a[i]] = cols_vals[0:,i]
    #print((cols_vals[i,:]))
    #print(columns_18[i])
        data_2018_df = pd.DataFrame(temp_dict)
        #columns_18 = cols_vals[0,:]
        
        #data_2018_df = pd.DataFrame(data=data_18,columns=columns_18)
    

    
        month = sheet_to_df_map_18[key].iloc[3,1:5].values[0:2]
        year = sheet_to_df_map_18[key].iloc[3,1:5].values[2:4]
        lga = sheet_to_df_map_18[key].iloc[1,0:5].values[0:2]
        state = sheet_to_df_map_18[key].iloc[1,0:5].values[2:4]
        sheet_to_df_map_18[key].iloc[0:2,0:5].values
        month_column = month[0]
        month_value = month[1]
        year_column = year[0]
        year_value = year[1]
        lga_column = lga[0]
        lga_value = lga[1]
        state_column = state[0]
        state_value = state[1]
        data_2018_df[state_column] = state_value
        data_2018_df[year_column] = year_value
        data_2018_df[month_column] = month_value
        data_2018_df[lga_column] = lga_value       
        data_2018_df_2=data_2018_df.drop('Sr No', axis=1)
        #df = df.drop(col_name, axis=1)
        #df_18=df_18.dropna()
        return data_2018_df_2[data_2018_df_2['Disease Name'].notnull()]
    
    def create_df_from_xls(self, filename):
        xls = self.read_excel_file(filename)
        sheet_map = self.parse_excel_sheets(xls)
        df_list12 = self.create_df_12(sheet_map)
        df_12 =    self.combine_df(df_list12)
        return df_12

